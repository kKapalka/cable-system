﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableSpawner : MonoBehaviour {

    public int CableLength;
    List<GameObject> allSegments;
	void Start () {
        allSegments = new List<GameObject>();
        GameObject go = (GameObject)Instantiate(Resources.Load("Cable_Start"));
        allSegments.Add(go);
        for(int i = 0; i < CableLength-1; i++)
        {
            go = (GameObject)Instantiate(Resources.Load("Cable_Segment"));
            allSegments.Add(go);
        }
        go = (GameObject)Instantiate(Resources.Load("Cable_End"));
        allSegments.Add(go);
        for(int i = 0; i < allSegments.Count - 1; i++)
        {
            allSegments[i].transform.parent = this.transform;
            allSegments[i].GetComponent<SpringJoint>().connectedBody = allSegments[i + 1].GetComponent<Rigidbody>();
            allSegments[i].GetComponent<CableComponent>().endPoint = allSegments[i + 1].transform;
        }
        go.transform.parent = this.transform;
    }
	
}
