﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlugScript : MonoBehaviour {

    private Vector3 worldPos, offset;
    private Transform otherEnd;
    private float maxLength;
    private void Start()
    {
        if (GetComponent<Rope_Line>() != null)
            setOtherEnd(GetComponent<Rope_Line>().target.transform);
    }
    public void setOtherEnd(Transform otherEnd)
    {
        this.otherEnd = otherEnd;
        maxLength = Vector3.Distance(transform.position, otherEnd.transform.position);
        Debug.Log("Cable used: " + Mathf.RoundToInt(maxLength));
    }
    private void OnMouseDown()
    {
        worldPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, worldPos.z));
    }
    
    private void OnMouseDrag()
    {
        worldPos.z += Input.mouseScrollDelta.y / 20;
        Vector3 curPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, worldPos.z));

        if (Vector3.Distance(curPos, otherEnd.transform.position) < maxLength+0.1f)
            transform.position = curPos;
        else
            transform.position = otherEnd.transform.position + ((curPos-otherEnd.transform.position).normalized * maxLength);

    }
}
